/**
  * Simple use of a QAbstractListModel that wraps a QVector of
  * a basic struct called MyCustomData.
  */
#ifndef MYCHARTDATA_H
#define MYCHARTDATA_H

#include <QObject>
#include <QAbstractListModel>

// Our struct we'll use in a QVector
struct MyCustomData {
    double value = 0.0;
    bool dataIsGood = true;
};

class MyChartData : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MyChartData(QObject *parent = 0);
    void addMoreData();     // add some more dummy data
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

signals:

public slots:

private:
    QVector<MyCustomData> m_data;

};


#endif // MYCHARTDATA_H
