/**
  * This is a demonstration of the behaviour of a QML Chart that uses
  * VBarModelWrapper to get its data from a QAbstractListModel-dervice class.
  *
  * Even if you add data to the model, the chart never updates with the new
  * information.
  *
  * by: John Woltman
  */
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtCharts 2.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("ChartModelMapperTest")

    // ListView works great
    Text {
        x: 0
        y: 0
        text: "ListView of Rectangles"
    }

    ListView {
        model: myChartData
        delegate: bunchOfBoxes
        x: 0
        y: 20
        width: 160
        height: parent.height - 20
    }
    Component {
        id: bunchOfBoxes
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: 5;
            color: "blue"
            border.width: 1
        }
    }

    // The chart and VBarModelMapper, doesn't update
    ChartView {
        title: "Bar series"
        x: 160
        y: 0
        height: parent.height
        width: 480
        antialiasing: true
        BarSeries {
            id: mySeries
            VBarModelMapper {
                id: chartDataMapper
                model: myChartData
                firstBarSetColumn: 0
                firstRow: 0
                lastBarSetColumn: 0
            }
        }
    }
}
