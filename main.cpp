/**
  * This is a demonstration of the behaviour of a QML Chart that uses
  * VBarModelWrapper to get its data from a QAbstractListModel-dervice class.
  *
  * Even if you add data to the model, the chart never updates with the new
  * information.
  *
  * by: John Woltman
  */
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "MyChartData.h"
int main(int argc, char *argv[])
{
    // Boring book-keeping stuff first
    qmlRegisterType<MyChartData>("com.chartmodelmappertest.qml", 1, 0, "MyChartData");
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    QQmlComponent component(&engine, QUrl(QLatin1String("qrc:/main.qml")));
    QQmlContext * context= engine.rootContext();

    // Make a new instance of MyChartData and use it as a context property in
    // the QML.
    MyChartData * chartData = new MyChartData(&app);
    context->setContextProperty("myChartData", chartData);

    // Once a second, add some more data to the model to show that the
    // ListView is working and the ChartView is not.
    QTimer * addItemsTimer = new QTimer(&app);
    QObject::connect(addItemsTimer, &QTimer::timeout, [chartData]() {
        chartData->addMoreData();
    });
    addItemsTimer->start(1000);

    component.create();
    return app.exec();
}
