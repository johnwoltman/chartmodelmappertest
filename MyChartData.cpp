#include "MyChartData.h"

MyChartData::MyChartData(QObject *parent) : QAbstractListModel(parent)
{
    // Populate some dummy data
    for (int i = 0; i < 12; i++) {
        MyCustomData d;
        d.value = 0.1 + i;
        m_data.append(d);
    }
}

void MyChartData::addMoreData()
{
    MyCustomData d;
    d.value = 1.21;
    beginInsertRows(QModelIndex(), m_data.count(), m_data.count());
    m_data.insert(m_data.count(), d);
    endInsertRows();
}

int MyChartData::rowCount(const QModelIndex &parent) const
{
    return m_data.count();
}

QVariant MyChartData::data(const QModelIndex &index, int role) const
{
    if (index.row() < m_data.count() ) {
        return m_data.at(index.row()).value;
    }
    return QVariant();
}

